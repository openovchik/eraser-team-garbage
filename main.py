import disnake
import random
import asyncio
from config import settings
from disnake.ext import commands
from disnake import ButtonStyle  #импорты библиотек

intents = disnake.Intents.default()
intents.message_content = True  #просто интенты, сюда можно не смотреть
client = commands.Bot(intents=intents)  #задача префикс и интентов
client.remove_command('help')  #удаление стандартного help

help_embed = disnake.Embed(color=0x430997, title="кам анды", description="нажми на кнопку с нужной категорией пон")  #embed для help
balls_color = ["🔴", "🟠", "🟡", "🟢", "🔵", "🟣", "🟤", "⚪️"]  #шары для ёлки

kvin = settings['kvin']
sgm = settings['sigma']
bst = settings['beast']


@client.event
async def on_ready():
  while True:
    await client.change_presence(status=disnake.Status.dnd, activity=disnake.Game("/негры"))

class RowButtons(disnake.ui.View):

  def __init__(self):
    super().__init__(timeout=None)

  @disnake.ui.button(label="Развлечения", style=ButtonStyle.blurple)
  async def fun_button(self, button: disnake.ui.Button, inter: disnake.MessageInteraction):

    embed = disnake.Embed(color=0x7A6EFF, title="ну тип развлечения", description="/димка\n/квин\n/бист\n/фразы\n/правила\n/нык\n/ёлка*\n/nig")
    embed.set_author(name="*-корректное отображение команды было замечено на Ubuntu 22.04, Linux Mint 21, Android 12/13 и Windows 10")
    await inter.response.send_message(embed=embed)

  @disnake.ui.button(label="Разное", style=ButtonStyle.blurple)
  async def defferent_button(self, button: disnake.ui.Button, inter: disnake.MessageInteraction):
    await inter.response.send_message(embed=disnake.Embed(color=0x808080, title="палезная категория команд", description="/фразы\n/базар\n/базарчик\n/яицо\n/слажить"))

  @disnake.ui.button(label="Мини-игры", style=ButtonStyle.grey)
  async def minigames(self, button: disnake.ui.Button, inter: disnake.MessageInteraction):
    await inter.response.send_message(embed=disnake.Embed(color=0x009900, title="Мини игры пон", description="/нумбер"))


class dim(disnake.ui.View):

  def __init__(self):
    super().__init__(timeout=None)

  @disnake.ui.button(label="ещё", style=ButtonStyle.grey)
  async def more(self, button: disnake.ui.Button, ctx):
    rndm = random.randint(1, 45)
    await ctx.send(file=disnake.File(f"C:/Users/Admin B/Desktop/софт/bot/dd/{rndm}.mp4"), view=dim())


class sigma(disnake.ui.View):

  def __init__(self):
    super().__init__(timeout=None)

  @disnake.ui.button(label="ещё", style=ButtonStyle.grey)
  async def more(self, button: disnake.ui.Button, inter: disnake.MessageInteraction):
    await inter.response.send_message(random.choice(sgm), view=sigma())
    
class beast(disnake.ui.View):

  def __init__(self):
    super().__init__(timeout=None)

  @disnake.ui.button(label="ещё", style=ButtonStyle.grey)
  async def more(self, button: disnake.ui.Button, inter: disnake.MessageInteraction):
    await inter.response.send_message(random.choice(bst), view=beast())  #кнопки


#команды

@client.slash_command(description="тип ваше сабщение от бота")
async def базарчик(ctx, text):
  await ctx.send(text)
  
@client.slash_command(description="просто ёлочка")
async def ёлка(message):
  
      msg = await message.channel.send(
    '```kotlin\n"\n                  **\n                 *' +
    random.choice(balls_color) + '️*\n                **' +
    random.choice(balls_color) + '**\n               ' +
    random.choice(balls_color) + '**' + random.choice(balls_color) +
    '**\n              ***' + random.choice(balls_color) + '***' +
    random.choice(balls_color) + '\n             ***' +
    random.choice(balls_color) + '**' + random.choice(balls_color) +
    '***\n            *' + random.choice(balls_color) + '**' +
    random.choice(balls_color) + '**' + random.choice(balls_color) + '*' +
    random.choice(balls_color) + '\n        спасибо за выбор амка бота"```')
      while True:
        await asyncio.sleep(0.5)
        await msg.edit(
      content='```kotlin\n"\n                  **\n                 *' +
      random.choice(balls_color) + '️*\n                **' +
      random.choice(balls_color) + '**\n               ' +
      random.choice(balls_color) + '**' + random.choice(balls_color) +
      '**\n              ***' + random.choice(balls_color) + '***' +
      random.choice(balls_color) + '\n             ***' +
      random.choice(balls_color) + '**' + random.choice(balls_color) +
      '***\n            *' + random.choice(balls_color) + '**' +
      random.choice(balls_color) + '**' + random.choice(balls_color) + '*' +
      random.choice(balls_color) + '\n        спасибо за выбор амка бота"```')

@client.slash_command(description="красивый базар")
async def базар(ctx, title, description):
  await ctx.send(embed=disnake.Embed(color=0x009900, title=title, description=description))


@client.slash_command(description="яицо каторае отвечает да иль нет")
async def яицо(ctx, question):

  list = ["да", "нет", "хз", "наверн"]
  otvet = random.choice(list)
  await ctx.send(embed=disnake.Embed(color=0x00BB2D,
                                     title=otvet,
                                     description="Вопрос: " + question +
                                     "\n**Ответ: " + otvet + "**"))


@client.slash_command(
  description="С помощью данной команды можно сложить два числа(целые)")
async def слажить(ctx, number1, number2):

  num_1 = int(number1)  #значение из первого числа
  num_2 = int(number2)  #значение из второго числа
  plus = num_1 + num_2 #сложение
  await ctx.send(plus)  #высылаение результата


@client.slash_command(description="команды")
async def негры(ctx):
  await ctx.send(embed=help_embed, view=RowButtons())


@client.slash_command(description="30+ оттенков 'я лублу разный'")
async def димка(ctx):
  rndm = random.randint(1, 45)
  await ctx.send(file=disnake.File(f"C:/Users/Admin B/Desktop/софт/bot/dd/{rndm}.mp4"), view=dim())


@client.slash_command(description="квинка слоумо")
async def квин(ctx):

  video_kvin = random.choice(kvin)
  await ctx.send(video_kvin)


@client.slash_command(description="правила елитнага раёна")
async def правила(ctx):
  await ctx.send("https://cdn.discordapp.com/attachments/1087026822442663936/1087732626892279838/ApOvcAmClWo.jpg")


@client.slash_command(description="типа рандом фразы")
async def фразы(ctx):

  list = ["нык пидор, доброе утро не сказал", "ниактив блог", "анчус эт рыба такая", "пон", "аххахаа шлюха штопаная", "кату кайфва.пнг", "иди нахуй, сука", "мне 9 пон", "кэфтэмэ", "да я люблю сосать член", "это пенис", "coems🤑", "я люблю разный", "нык ебет баранов только в общем чате"]
  await ctx.send(random.choice(list))


@client.slash_command()
async def nig(ctx, emoji_or_text):
  await ctx.send("<:nigga1:1061653683672207490>" + emoji_or_text + "<:nigga2:1061654037495300096>")


@client.slash_command()
async def нык(ctx):
  await ctx.send("https://cdn.discordapp.com/attachments/1087026822442663936/1113317889408249856/43_20230531090616.png")


@client.slash_command(description="типа угадай цыфру")
async def нумбер(ctx, number=None):
  numbers = ["1", "2", "3", "4", "5", "6"]

  if number == random.choice(numbers):
    await ctx.send("Поздравляю, вы выйграли")
  else:
    await ctx.send("Вы не угадали")


@client.slash_command(name="на_чили_на_расбони", description="начили на расладони")
async def чил(ctx):
  await ctx.send("https://cdn.discordapp.com/attachments/1087026822442663936/1121091754347728997/online-video-cutter.com.mp4")


@client.slash_command(description="сёмга с сыром")
async def симга(ctx):
  await ctx.send(random.choice(sgm), view=sigma())
  
@client.slash_command(description="я сброшу на вас 250 тычяс тонн тротила")
async def бист(ctx):
  await ctx.send(random.choice(bst), view=beast())

client.run("ur_token_here")  #запуск бота